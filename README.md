This is a shopping application with host as the seller and client as the buyer. The buyer needs to obtain the seller's (the store's) IP to "visit" the store. The seller can manage the stocks of items and get notified when something is purchased. 

This project is a part of Object-Oriented Programming II subject  of Kasetsart University. Created by class of 2018 software and knowledge engineering students:  

5710546364 Pitchaya Namchaisiri and 5710547212 Woramate Jumroonsilp 

under supervision of James Brucker.