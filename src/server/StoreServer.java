package server;

import java.io.IOException;
import java.util.ArrayList;

import ui.StoreMain;
import StockAndStore.Storable;
import StockAndStore.Store;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class StoreServer extends AbstractServer{
	ArrayList<ConnectionToClient> loggedIn;
	String receiverName = null;
	public StoreServer(int port) {
		super(port);
		loggedIn = new ArrayList<ConnectionToClient>();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message = (String) msg;
		StoreMain.addText("Received " + message);
		String[] command = message.split(" ");
		if (command[0].equals("getStock")) {
			StoreMain.addText("Received stock request from " + client.toString()); 
			try {
				client.sendToClient(Store.getInstance().getStock());
				StoreMain.addText("Sent stock information to " + client.toString());
			} catch (IOException e) {
				StoreMain.addText("Failed sending stock information to " + client.toString());
				System.out.println(e.toString());
			}
		}
		else if (command[0].equals("buy")) { 
			StoreMain.addText("Received buy request from " + client.toString());
			try {
				client.sendToClient(Store.getInstance().getStock().sell(command[1], Integer.parseInt(command[2])));
			} catch (NullPointerException | NumberFormatException e) {
				try {
					client.sendToClient("buyinformationerror");
				} catch (IOException e1) {
					StoreMain.addText("Failed sending error message to " + client.toString());
				}
			} catch (IOException e2) {
				StoreMain.addText("Failed selling item to " + client.toString());
				System.out.println(e2.toString());
				try {
					client.sendToClient("buyerror");
				} catch (IOException e) {
					StoreMain.addText("Failed sending error message to " + client.toString());
				}
			}
		}
	}
	@Override
	protected void clientConnected(ConnectionToClient client) {
		StoreMain.addText("User " + client.toString() + " has connected");
		loggedIn.add(client);
	}
	@Override
	protected synchronized void clientDisconnected(ConnectionToClient client) {
		StoreMain.addText("User " + client.toString() + " disconnected");
		loggedIn.remove(client);
	}
	public static void main(String[] args) {
		final int port = 2745;
		StoreServer cs = new StoreServer(port);
		try {
			cs.listen();
		} catch (IOException e) {
			System.out.println("Failed to start server.");
			System.out.println(e);
			System.exit(1);
		}
		System.out.println("Server host is 158.108.143.169");
		System.out.println("Server listening to port " + port);
	}
}
