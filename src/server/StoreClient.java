package server;

import java.io.IOException;
import java.util.Scanner;

import ui.CustomerMain;
import StockAndStore.Client;
import StockAndStore.Stock;
import StockAndStore.Storable;

import com.lloseng.ocsf.client.AbstractClient;


public class StoreClient extends AbstractClient{
	public StoreClient(String host, int port) {
		super(host, port);
	}

	@Override
	protected void handleMessageFromServer(Object message) {
		if (message != null && message.getClass() == Stock.class) {
			Stock sentStock = (Stock) message;
			Client.getInstance().setStock(sentStock);
			CustomerMain.addText("Received stock data from " + this.getHost());
		}
		else if (message != null && message instanceof Storable) {
			Storable item = (Storable) message;
			Client.getInstance().addToInventory(item);
			CustomerMain.addText("Purchase successful. " + item.getAmount() + " " + item.getName() + " is now in your inventory.");
		}
		else if (((String)message).equals("buyerror")) {
			CustomerMain.addText("Failed buying from store. Please try again.");
		}
	}

	@Override
	protected void connectionClosed() {
		System.out.println("Disconnected");
	}

	@Override
	protected void connectionEstablished() {
		System.out.println("Connected to " + super.getHost());
	}
	public static void main(String[] args) throws IOException {
		Scanner console = new Scanner(System.in);
		String server = "158.108.143.169";
		int port = 2745;
		StoreClient client = new StoreClient(server,port);
		client.openConnection();
//		System.out.println("Your IP is " + client.getInetAddress());
		while (client.isConnected()) {
			String msg = console.nextLine().trim();
			if (msg.equalsIgnoreCase("/quit")) client.closeConnection();
			else client.sendToServer(msg);
		}
		console.close();
	}


}
