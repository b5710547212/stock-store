package StockAndStore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import server.StoreClient;
import ui.CustomerMain;

public class Client {
	private Stock stock;
	private StoreClient client;
	private List<Storable> inventory;
	private static Client instance = new Client();
	private Client() {
		stock = null;
		inventory = new ArrayList<Storable>();
	}
	public void addToInventory(Storable item) {
		this.inventory.add(item);
	}
	public synchronized static Client getInstance() {
		return instance;
	}
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	public Stock getStock() {
		return this.stock;
	}
	public StoreClient getClient() {return this.client;}
	public void setClient(StoreClient client) {
		this.client = client;
	}
	public void requestStock() {
		try {
			client.sendToServer("getStock");
		} catch (IOException e) {
			CustomerMain.addText("Failed requesting stock from the store.");
		}
	}
	public String getInventoryInfo() {
		String detail = "";
		for (Storable item : this.inventory) {
			for (int i = 0; i < item.getAmount(); i++) {
				detail += String.format("ID: %s Name: %s Price: %.2f Volume: %.2f Weight: %.2f Category: %s\n", item.getId(), item.getName(), item.getPrice(), item.getVolume(), item.getWeight(), item.getCategory());
			}
		}
		return detail;
	}

	
}
