package StockAndStore;

import java.io.Serializable;

public class Product implements Storable, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id, name, category;
	double price, volume, weight;
	int amount;
	public Product(String id, String name, double price, double volume, double weight, String category) {
		this.id = this.genId( id );
		this.name = name;
		this.price = price;
		this.volume = volume;
		this.weight = weight;
		this.category = category;
		this.amount = 0;
	}
	public String genId( String input ) {
		String pro = input;
		if( input.charAt( 0 ) == 'b' && input.charAt( input.length()-1 ) == 'b' );
		else  pro = "b" + input + "b";
		return pro;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String getId() {return id;}

	@Override
	public String getName() {return name;}

	@Override
	public String getCategory() {return category;}

	@Override
	public double getPrice() {return price;}

	@Override
	public double getWeight() {return weight;}

	@Override
	public double getVolume() {return volume;}

	@Override
	public void setPrice(double price) { this.price = price;}

	@Override
	public void setWeight(double weight) { this.weight = weight;}

	@Override
	public void setVolume(double volume) { this.volume = volume;}
	
	public String toString() {
		return String.format("%s %s", this.id, this.name);
	}
}
