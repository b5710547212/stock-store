package StockAndStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StaffLogin {

	private File account = new File( "res/staffAccount.txt" );

	public StaffLogin(){}
	
	public int login( String id, String password ) {
		List<String> temp = this.readAccount();
		for ( int i = 0 ; i < temp.size() ; i++ ) {
			String[] tempIn = temp.get( i ).split( " " );
			if ( id.equalsIgnoreCase( tempIn[0] ) ) {
				if ( password.equals( this.decrypt( tempIn[ 1 ] ) ) ) {
					this.writeAccount( temp );
					return 1;
				}
				this.writeAccount( temp );
				return 0;
			}
		}
		this.writeAccount( temp );
		return -1;
	}
	
	public int register( String id, String password ) {
		List<String> temp = this.readAccount();
		if ( !this.chkId( id ) ) {
			temp.add( id + " " + this.encrypt( password ) );
			this.writeAccount( temp );
			return 1;
		} return -1;
	}

	public boolean chkId( String id ) {
		List<String> temp = this.readAccount();
		boolean ans = false;
		for ( int i = 0 ; i < temp.size() ; i++ ) {
			String[] tempIn = temp.get( i ).split( " " );
			ans = id.equalsIgnoreCase( tempIn[0] ); 
		}
		this.writeAccount( temp );
		return ans;
	}
	
	public List<String> readAccount() {
		List<String> temp = new ArrayList<String>();
		try {
			if (account.exists() ) {
				Scanner scanAccount = new Scanner( account );
				while ( scanAccount.hasNext() ) {
					temp.add( scanAccount.nextLine() );
				}
			}
		} catch ( Exception e ) { System.out.println( e ); }
		return temp;
	}
	
	public void writeAccount( List<String> write ) {
		try {
			FileWriter accountFw = new FileWriter( this.account );
			PrintWriter accountPw = new PrintWriter( accountFw );
			for ( int i = 0 ; i < write.size() ; i++ ) {
				accountPw.println( write.get( i ) );
			} 
			accountPw.close();
			accountFw.close();
		} catch ( Exception e ) {
			System.out.println( e );
		}
	}
	
	public String encrypt( String raw ) {
		String encrypt = "";
		for ( int i =0 ; i < raw.length() ; i++ ) {
			encrypt += (int) raw.charAt( i ) + "/";
		}
		return encrypt;
	}

	public String decrypt( String encrypted ) {
		String[] code = new String[ encrypted.length() ];
		code = encrypted.split( "/" );
		String spell = "";
		for ( int i = 0 ; i < code.length ; i++ ) {
			int tempIn = Integer.parseInt( code[ i ] );
			spell += (char) tempIn;
		}
		return spell;
	}

	public static void main(String[] args) {
		StaffLogin sl = new StaffLogin();
		System.out.println( sl.login( "woramate", "Thd4554" ) );
	}
	
}