package StockAndStore;

public interface Storable {
	
	//accessor
	public String getId();
	public String getName();
	public String getCategory();
	public double getPrice();
	public double getWeight();
	public double getVolume();
	public int getAmount();
	
	//mutator
	public void setPrice( double price );
	public void setWeight( double weight );
	public void setVolume( double volume );
	public void setAmount(int amount);
	
	//additional
//	public List<Double> getDiscountRate( List<Double> discountRate );
//	public void setDiscountRate();
	
}
