package StockAndStore;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Woramate Jumroonsilp
 * @author Pitchaya Namchaiiri
 *
 */
public class Stock implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File registeredProduct = new File( "res/registeredProduct.txt" );
	private final int capacity;

	/**
	 * Constructor for stock.
	 * @param capacity of stock
	 */
	public Stock( int capacity ) {

		this.capacity = capacity;

	}

	public String[] getProductList() {
		List<Storable> temp = this.readRegisteredProduct();
		List<String> categoryList = new ArrayList<String>();
		for(Storable thisItem : temp) {
			if (!categoryList.contains(thisItem.getName())) {
				categoryList.add(thisItem.getName());
			}
		}
		String[] arr = new String[categoryList.size()];
		this.writeRegisteredProduct( temp );
		return categoryList.toArray(arr);
	}

	/**
	 * Add product to stock.
	 * @param product
	 * @return true, add complete. false, add fail
	 */
	public boolean registerProduct( Storable storable ) {
		if( isFull() ) {
			return false;
		}
		else {
			List<Storable> temp = this.readRegisteredProduct();
			temp.add( storable );
			this.writeRegisteredProduct( temp );
		}
		return true;
	}

	/**
	 * If this stock is full, return true. Else, false.
	 * @return Full, true. Else, false.
	 */
	public boolean isFull() {return this.getQuantity() >= this.getCapacity();}

	/**
	 * Sell product 
	 * @return
	 */
	public Storable sell( String key, int amount ) {

		if ( this.getType( key ) > 0 ) {
			List<Storable> temp = this.readRegisteredProduct();
			for ( int i = 0 ; i < temp.size() ; i++ ) {
				if ( temp.get( i ).getId().equals( key ) ) {
					temp.get( i ).setAmount( temp.get( i ).getAmount()-amount );
					this.writeRegisteredProduct( temp );
					Product pro = new Product( temp.get( i ).getId(), temp.get( i ).getName(), temp.get( i ).getPrice(), temp.get( i ).getVolume(), temp.get( i ).getWeight(), temp.get( i ).getCategory() );
					pro.setAmount( amount );
					return pro;
				}
			} return null;
		} else if ( this.getType( key ) < 0 ) {
			List<Storable> temp = this.readRegisteredProduct();
			for ( int i = 0 ; i < temp.size() ; i++ ) {
				if ( temp.get( i ).getName().equals( key ) ) {
					temp.get( i ).setAmount( temp.get( i ).getAmount()-amount );
					this.writeRegisteredProduct( temp );
					Product pro = new Product( temp.get( i ).getId(), temp.get( i ).getName(), temp.get( i ).getPrice(), temp.get( i ).getVolume(), temp.get( i ).getWeight(), temp.get( i ).getCategory() );
					pro.setAmount( amount );
					return pro;
				}
			} return null;
		} return null;
	}

	/**
	 * Find product in stock.
	 * @param storable
	 * @return true, found storable. Else, not.
	 */
	public Storable findProductName( String key ) { 
		List<Storable> temp = this.readRegisteredProduct();
		for ( int i = 0 ; i < temp.size() ; i++ ) {
			if ( temp.get( i ).getName().equalsIgnoreCase( key ) ) {
				this.writeRegisteredProduct( temp );
				return temp.get( i );
			}
		} 
		this.writeRegisteredProduct( temp );
		return null;
	}

	public Storable findProductId( String key ) { 
		List<Storable> temp = this.readRegisteredProduct();
		for ( int i = 0 ; i < temp.size() ; i++ ) {
			if ( temp.get( i ).getId().equalsIgnoreCase( key ) ) {
				this.writeRegisteredProduct( temp );
				return temp.get( i );
			}
		}return null;
	}

	/**	
	 * Unregister storable from stock.
	 * @param storable
	 */
	public void unregisterProduct( Storable storable ) { 
		List<Storable> temp = this.readRegisteredProduct();
		Storable selected = null; 
		for (Storable item : temp) {
			if (item.getId().equals(storable.getId())) {
				selected = item;
			}
		}
		if (selected != null) temp.remove(selected);
		this.writeRegisteredProduct( temp );
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public int getType( String key ) {

		if( key.charAt( 0 ) == 'b' && key.charAt( key.length()-1 ) == 'b' ) {
			return 1;
		} return -1;
	}

	/**
	 * return capacity of this stock
	 * @return capacity
	 */
	public int getCapacity() { return this.capacity; }

	/**
	 * return amount of product in stock
	 * @return quantity
	 */
	public int getQuantity() {
		List<Storable> temp = this.readRegisteredProduct();
		int quantity = 0;
		for (Storable item : temp) {
			System.out.println( item.getAmount() );
			quantity += item.getAmount();
		}
		this.writeRegisteredProduct( temp );
		return quantity;
	}

	/**
	 * Gets the remaining slots of this stock. 
	 * @return remaining slots for this stock.
	 */
	public int getRemaining() {
		return this.getCapacity() - this.getQuantity();
	}
	
	public void addProduct(Storable product, int amount) {
		List<Storable> temp = this.readRegisteredProduct();
		for(Storable item : temp) {
			if (product.getId().equals(item.getId())) {
				item.setAmount(item.getAmount() + amount);
			}
		}
		this.writeRegisteredProduct(temp);
	}
	
	public void removeProduct(Storable product, int amount) {
		List<Storable> temp = this.readRegisteredProduct();
		for(Storable item : temp) {
			if (product.getId().equals(item.getId())) {
				item.setAmount(item.getAmount() - amount);
			}
		}
		this.writeRegisteredProduct(temp);
	}


	public List<Storable> readRegisteredProduct() {
		List<Storable> temp = new ArrayList<Storable>();
		try {
			if ( registeredProduct.exists() ) {
				Scanner scanRegisteredProduct = new Scanner( registeredProduct );
				while ( scanRegisteredProduct.hasNext() ) {
					String[] tempIn = scanRegisteredProduct.nextLine().split( " " );
					Storable item = new Product( tempIn[0], tempIn[1], Double.parseDouble( tempIn[2] ), Double.parseDouble( tempIn[3] ), Double.parseDouble( tempIn[4] ), tempIn[5] );
					item.setAmount( Integer.parseInt( tempIn[ 6 ] ) );
					temp.add( item );					
				} scanRegisteredProduct.close();
			}
		} catch ( Exception e ) { 
			System.out.println("ReadError");
			System.out.println( e ); 
		}
		return temp;
	}

	public void writeRegisteredProduct( List<Storable> write ) {
		try {
			FileWriter registeredProductFw = new FileWriter( registeredProduct );
			PrintWriter registeredProductPw = new PrintWriter( registeredProductFw );
			for ( int i = 0 ; i < write.size() ; i++ ) {
				registeredProductPw.println( write.get( i ).getId() + " " + write.get( i ).getName() + " " + write.get( i ).getPrice() + " " + write.get( i ).getVolume() + " " + write.get( i ).getWeight() + " " + write.get( i ).getCategory() + " " +write.get( i ).getAmount() );
			}
			registeredProductFw.close();
			registeredProductPw.close();
		} catch ( Exception e ) {
			System.out.println("WriteError");
			System.out.println( e ); 
		}
	}

	/**
	 * Gets detail of this stock.
	 * @return details of this stock in lines. Does not include those with zero amount.
	 */
	public String toString() {
		List<Storable> temp = this.readRegisteredProduct();
		String detail = "";
		for (Storable item : temp) {
			for (int i = 0; i < item.getAmount(); i++) {
				detail += String.format("ID: %s Name: %s Price: %.2f Volume: %.2f Weight: %.2f Category: %s\n", item.getId(), item.getName(), item.getPrice(), item.getVolume(), item.getWeight(), item.getCategory());
			}
		}
		this.writeRegisteredProduct( temp );
		return detail;
	}

	public static void main(String[] args) {
		Stock st = new Stock(100);
		System.out.println( st.toString() );
	}

}