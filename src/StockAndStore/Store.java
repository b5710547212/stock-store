package StockAndStore;

import java.io.IOException;

import javax.security.sasl.SaslServer;

import server.StoreServer;
import ui.StoreMain;

/**
 * Store for selling items. 
 * @author Pitchaya Namchaisiri
 * @author Woramate Jumroonsilp
 *
 */
public class Store {
	/**
	 * Stock that this store holds.
	 */
	private Stock stock;
	/**
	 * Instance of this Store.
	 */
	private static StoreServer server;
	private static Store instance = new Store();
	/**
	 * Constructor
	 */
	private Store(){
		stock = new Stock(100);
		final int port = 3000;
		server = new StoreServer(port);
	}
	public static void connect() {
		try {
			server.listen();
			StoreMain.addText("Initiated server");
		} catch (IOException e) {
			StoreMain.addText("Failed initiating server. Please restart application.");
			StoreMain.addText(e.toString());
		}

	}
	/**
	 * Gets this Store's instance.
	 * @return this Store's instance.
	 */
	public static synchronized Store getInstance() {
		return Store.instance;
	}
	/**
	 * Gets this store's current stock of items.
	 * @return stock in this store.
	 */
	public Stock getStock() {
		return stock;
	}
	/**
	 * Prevents Store from being cloneable.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
}
