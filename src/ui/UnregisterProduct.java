package ui;

import java.awt.Container;
import java.awt.GridLayout;



import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



import StockAndStore.Storable;
import StockAndStore.Store;

public class UnregisterProduct extends JFrame implements Runnable{
	JComboBox<String> categoryBox;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public UnregisterProduct() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(500, 200);
	}
	private void initComponents() {
		this.categoryBox = new JComboBox<String>(Store.getInstance().getStock().getProductList());
		Container mainPanel = this.getContentPane();
		JButton unregister = new JButton("Unregister");
		JLabel title = new JLabel("unregister product");
		JLabel cateLbl = new JLabel("Select product");
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel infoGrid = new JPanel();
		infoGrid.setLayout(new GridLayout(1,2));
		
		infoGrid.add(cateLbl);
		infoGrid.add(categoryBox);
		
		mainPanel.add(title);
		title.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(infoGrid);
		mainPanel.add(unregister);
		unregister.setAlignmentX(CENTER_ALIGNMENT);
		
		unregister.addActionListener(p -> {
			String selected = (String) this.categoryBox.getSelectedItem();
			Storable selectedItem = Store.getInstance().getStock().findProductName(selected);
			try {
				int choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to unregister this product? \n This will also remove the product form the stock.", "Are you sure?", JOptionPane.YES_NO_OPTION);
				if (choice == JOptionPane.YES_OPTION) {
					Store.getInstance().getStock().unregisterProduct(selectedItem);
					StoreMain.addText("Successfully unregistered " + selected);
					StoreMain.addText("Stock now has " + Store.getInstance().getStock().getQuantity() + " items out of " + Store.getInstance().getStock().getCapacity() + " items");
					this.dispose();
				}
			} catch(NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Item not found. Please check if you have registered the product.");
			}
		});
	}

}
