package ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.EventHandler;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import StockAndStore.Client;

public class CustomerMain extends JFrame implements Runnable {
	static JTextArea console;
	static JButton connect;
	static JButton disconnect;

	@Override
	public void run() {
		this.setVisible(true);
		
	}
	public CustomerMain() {
		this.initComponents();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	public static void addText(String text) {
		console.append("\n");
		console.append(text);
	}
	private void initComponents() {
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel sidePanel = new JPanel();
		sidePanel.setLayout(new GridLayout(5, 1));
		JButton viewStore = new JButton("View store");
		JButton refresh = new JButton("Refresh");
		JButton buy = new JButton("Buy");
		connect  = new JButton("Connect...");
		disconnect = new JButton("Disconnect");
		sidePanel.add(connect);
		sidePanel.add(disconnect);
		disconnect.setEnabled(false);
		sidePanel.add(viewStore);
		sidePanel.add(refresh);
		sidePanel.add(buy);
		
		console = new JTextArea("Logged in", 10, 30);
		console.setEditable(false);
		JScrollPane scroll = new JScrollPane(console);
		mainPanel.add(scroll);
		mainPanel.add(sidePanel);
		sidePanel.setAlignmentX(RIGHT_ALIGNMENT);
		
		connect.addActionListener(p -> {
			CustomerConnect connectui = new CustomerConnect();
			connectui.run();
		});
		
		disconnect.addActionListener(p -> {
			try {
				Client.getInstance().getClient().closeConnection();
				addText("Disconnected");
				connect.setEnabled(true);
				disconnect.setEnabled(false);
				CustomerMain.console.setBackground(Color.white);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Cannot disconnect. Please try again later.");
			}
		});
		
		viewStore.addActionListener(p -> {
			CustomerViewStore view = new CustomerViewStore();
			view.run();
		});
		
		refresh.addActionListener(p -> {
			Client.getInstance().setStock(null);
			Client.getInstance().requestStock();
		});
		
		buy.addActionListener(p -> {
			CustomerBuy custBuy = new CustomerBuy();
			custBuy.run();
		});
		
	}
	public static void main(String[] args) {
		CustomerMain ui = new CustomerMain();
		ui.run();
	}
}
