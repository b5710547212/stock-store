package ui;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import StockAndStore.Store;

public class StoreMain extends JFrame implements Runnable{
	static JTextArea console;
	public StoreMain() {
		this.initComponents();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		Store.connect();
	}
	public static void addText(String text) {
		console.append("\n");
		console.append(text);
	}
	private void initComponents() {
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel sidePanel = new JPanel();
		sidePanel.setLayout(new GridLayout(5, 1));
		JButton addNewProduct = new JButton("Register new product");
		JButton addProductToStock = new JButton("Add to stock");
		JButton viewStock = new JButton("View stock");
		JButton removeFromStock = new JButton("Remove from stock");
		JButton unregister = new JButton("Unregister product");
		sidePanel.add(addNewProduct);
		sidePanel.add(addProductToStock);
		sidePanel.add(viewStock);
		sidePanel.add(removeFromStock);
		sidePanel.add(unregister);
		
		console = new JTextArea("Logged in", 10, 100);
		console.setEditable(false);
		JScrollPane scroll = new JScrollPane(console);
		mainPanel.add(scroll);
		mainPanel.add(sidePanel);
		sidePanel.setAlignmentX(RIGHT_ALIGNMENT);
		
		addNewProduct.addActionListener(p -> {
			AddNewProduct register = new AddNewProduct();
			register.run();
		});
		
		addProductToStock.addActionListener(p -> {
			AddProduct add = new AddProduct();
			add.run();
		});
		viewStock.addActionListener(p -> {
			ViewStock view = new ViewStock();
			view.run();
		});
		removeFromStock.addActionListener(p -> {
			RemoveFromStock remove = new RemoveFromStock();
			remove.run();
		});
		unregister.addActionListener(p -> {
			UnregisterProduct urgt = new UnregisterProduct();
			urgt.run();
		});
	}
	@Override
	public void run() {
		this.setVisible(true);
		
	}
	public static void main(String[] args) {
		StoreMain main = new StoreMain();
		main.run();
	}
	
}
