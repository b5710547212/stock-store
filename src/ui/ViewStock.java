package ui;

import java.awt.Container;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import StockAndStore.Store;

import java.awt.GridLayout;

public class ViewStock extends JFrame implements Runnable{
	JTextPane disp;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public ViewStock() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(600, 300);
	}
	private void initComponents() {
		Container mainPanel = this.getContentPane();
		disp = new JTextPane();
		JScrollPane scroll = new JScrollPane(disp);
		disp.setAlignmentX(CENTER_ALIGNMENT);
		disp.setAlignmentY(CENTER_ALIGNMENT);
		mainPanel.setLayout(new GridLayout(1, 1, 20, 20));
		
		mainPanel.add(scroll);
		
		this.showStock();
	}
	private void showStock() {
		this.disp.setText(Store.getInstance().getStock().toString());
	}

}
