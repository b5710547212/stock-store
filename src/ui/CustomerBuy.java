package ui;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import StockAndStore.Client;
import StockAndStore.Storable;
import StockAndStore.Store;

public class CustomerBuy extends JFrame implements Runnable{
	JComboBox<String> categoryBox;
	JSpinner amountSpin;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public CustomerBuy() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
	}
	private void initComponents() {
		this.categoryBox = new JComboBox<String>(Client.getInstance().getStock().getProductList());
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel infoGrid = new JPanel();
		infoGrid.setLayout(new GridLayout(2,2));
		JLabel title = new JLabel("Buy");
		JLabel categLbl = new JLabel("Select product");
		SpinnerModel sm = new SpinnerNumberModel(0, 0, Client.getInstance().getStock().getRemaining(), 1);
		this.amountSpin = new JSpinner(sm);
		this.amountSpin.getEditor().setEnabled(false);
		JButton buyProduct = new JButton("Buy");
		JLabel amountLbl = new JLabel("Amount");

		infoGrid.add(categLbl);
		infoGrid.add(categoryBox);
		infoGrid.add(amountLbl);
		infoGrid.add(amountSpin);

		mainPanel.add(title);
		title.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(infoGrid);
		mainPanel.add(buyProduct);
		buyProduct.setAlignmentX(CENTER_ALIGNMENT);
		buyProduct.addActionListener(p -> {
			int amount = (Integer)this.amountSpin.getValue();
			String selected = (String)this.categoryBox.getSelectedItem();
			Storable selectedItem = Client.getInstance().getStock().findProductName(selected);
			try {
				String productID = selectedItem.getId();
				try {
					Client.getInstance().getClient().sendToServer("buy " + productID + " " + amount);
				} catch (Exception e) {
					CustomerMain.addText("Cannot contact store for buying. Please try again.");
				}
			} catch (NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Item not found. Please check again.");
			}
			
		});

	}

}
