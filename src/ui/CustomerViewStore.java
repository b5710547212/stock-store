package ui;

import java.awt.Container;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import StockAndStore.Client;

public class CustomerViewStore extends JFrame implements Runnable{
	JTextPane disp;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public CustomerViewStore() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(600, 300);
	}
	private void initComponents() {
		disp = new JTextPane();
		JScrollPane scroll = new JScrollPane(disp);
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new GridLayout(1,1));
		mainPanel.add(scroll);
		
		disp.setText(Client.getInstance().getStock().toString());
	}
	


}
