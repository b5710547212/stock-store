package ui;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import StockAndStore.Storable;
import StockAndStore.Store;

public class RemoveFromStock extends JFrame implements Runnable{
	JComboBox<String> categoryBox;
	JSpinner amountSpin;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public RemoveFromStock() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
	}
	private void initComponents() {
		this.categoryBox = new JComboBox<String>(Store.getInstance().getStock().getProductList());
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel infoGrid = new JPanel();
		infoGrid.setLayout(new GridLayout(2,2));
		JLabel title = new JLabel("Remove product from stock");
		JLabel categLbl = new JLabel("Select product");
		
		JButton addProduct = new JButton("Remove product");
		JLabel amountLbl = new JLabel("Amount");

		infoGrid.add(categLbl);
		infoGrid.add(categoryBox);
		infoGrid.add(amountLbl);
		try {
			SpinnerModel sm = new SpinnerNumberModel(0, 0, 
					Store.getInstance().getStock().findProductName((String)this.categoryBox.getSelectedItem()).getAmount()
					, 1);
			this.amountSpin = new JSpinner(sm);
			infoGrid.add(amountSpin);
		} catch (NullPointerException e) {
			this.dispose();
			JOptionPane.showMessageDialog(null, "There are no registered products yet. Please register them first.");
		}
		mainPanel.add(title);
		title.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(infoGrid);
		mainPanel.add(addProduct);
		addProduct.setAlignmentX(CENTER_ALIGNMENT);
		addProduct.addActionListener(p -> {
			int amount = (Integer)this.amountSpin.getValue();
			String selected = (String)this.categoryBox.getSelectedItem();
			Storable selectedItem = Store.getInstance().getStock().findProductName(selected);
			try {
				Store.getInstance().getStock().removeProduct(selectedItem, amount);
				StoreMain.addText("Removed " + amount + " " + selected);
				StoreMain.addText("Stock now has " + Store.getInstance().getStock().getQuantity() + " items out of " + Store.getInstance().getStock().getCapacity() + " items");
				this.dispose();
			} catch (NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Item not found. Please check if you have registered the product.");
			}

		});
		this.categoryBox.addActionListener(p -> {
			this.amountSpin.setModel(new SpinnerNumberModel(0, 0, 
					Store.getInstance().getStock().findProductName((String)this.categoryBox.getSelectedItem()).getAmount()
					, 1));
		});
	}

}
