package ui;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import StockAndStore.Product;
import StockAndStore.Store;

public class AddNewProduct extends JFrame implements Runnable{
	JTextField idTxt, nameTxt, priceTxt, volTxt, weightTxt, categoryTxt;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public AddNewProduct() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(300, 500);
	}
	private void initComponents() {
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JLabel title = new JLabel("Register new product");
		title.setAlignmentX(CENTER_ALIGNMENT);
		title.setFont(new Font("Agency FB", Font.BOLD, 26));
		JButton registerProduct = new JButton("Register product");
		registerProduct.setAlignmentX(CENTER_ALIGNMENT);
		JPanel detailGrid = new JPanel();
		detailGrid.setLayout(new GridLayout(6,2));
		this.idTxt = new JTextField();
		JLabel idLbl = new JLabel("ID");
		this.nameTxt = new JTextField();
		JLabel nameLbl = new JLabel("Product Name");
		this.priceTxt = new JTextField();
		JLabel priceLbl = new JLabel("Product Price");
		this.volTxt = new JTextField();
		JLabel volLbl = new JLabel("Volume");
		this.weightTxt = new JTextField();
		JLabel weightLbl = new JLabel("Weight");
		this.categoryTxt = new JTextField();
		JLabel categoryLbl = new JLabel("Product Category");

		detailGrid.add(idLbl); detailGrid.add(idTxt);
		detailGrid.add(nameLbl); detailGrid.add(nameTxt);
		detailGrid.add(priceLbl); detailGrid.add(priceTxt);
		detailGrid.add(volLbl); detailGrid.add(volTxt);
		detailGrid.add(weightLbl); detailGrid.add(weightTxt);
		detailGrid.add(categoryLbl); detailGrid.add(categoryTxt);

		mainPanel.add(title);
		mainPanel.add(detailGrid);
		mainPanel.add(registerProduct);

		registerProduct.addActionListener(p -> {
			try {
				Product newProduct = new Product(
						this.idTxt.getText(), this.nameTxt.getText(), Double.parseDouble(this.priceTxt.getText()), Double.parseDouble(this.volTxt.getText()), Double.parseDouble(this.weightTxt.getText()), this.categoryTxt.getText());
				Store.getInstance().getStock().registerProduct(newProduct);
				StoreMain.addText("Registered " + newProduct.toString() + " to the stock.");
				StoreMain.addText("Stock now has " + Store.getInstance().getStock().getQuantity() + " items out of " + Store.getInstance().getStock().getCapacity() + " items");
				this.dispose();
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Wrong data. Please check again.");
			}
		});
	}
}
