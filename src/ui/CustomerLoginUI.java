package ui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import StockAndStore.StaffLogin;

public class CustomerLoginUI extends JFrame implements Runnable{
	JPasswordField pwField;
	JTextField userField;
	JButton loginBtn;
	private StaffLogin sl;
	@Override
	public void run() {
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
	}

	public CustomerLoginUI() {
		this.initComponents();
	}
	private void initComponents() {

		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		JLabel noticeLbl = new JLabel("Please login");

		JPanel loginGrid = new JPanel();
		loginGrid.setLayout(new GridLayout(2,2));

		JLabel userLbl = new JLabel("Username");
		JLabel pwLbl = new JLabel("Password");

		this.pwField = new JPasswordField();
		this.userField = new JTextField(10);
		this.loginBtn = new JButton("Login");
		loginGrid.add(userLbl);
		loginGrid.add(this.userField);
		loginGrid.add(pwLbl);
		loginGrid.add(this.pwField);

		mainPanel.add(noticeLbl);
		mainPanel.add(loginGrid);
		mainPanel.add(loginBtn);
		ActionListener L = (p -> {
			String pw = "";
			for (char i : pwField.getPassword()) pw += i; 
			if ( sl.login( userField.getText(), pw ) > 0 ) {
				JOptionPane.showMessageDialog(null, "Welcome to StockAndStore, " + userField.getText());
				CustomerMain mainUi = new CustomerMain();
				mainUi.run();
				this.dispose();
			} else if ( sl.login( userField.getText(), pw ) == 0 ) {
				JOptionPane.showMessageDialog(null, "Wrong username or password");
			} else {
				JOptionPane.showMessageDialog(null, "User not found");
			}
		});
		this.loginBtn.addActionListener( L );
		this.pwField.addActionListener( L );
	}
	public static void main( String[] args ) {
		CustomerLoginUI cl = new CustomerLoginUI();
		cl.run();
	}
}


