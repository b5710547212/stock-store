package ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;






import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import StockAndStore.Client;
import server.StoreClient;


public class CustomerConnect extends JFrame implements Runnable{
	JTextField ipTxt, portTxt;
	@Override
	public void run() {
		this.setVisible(true);
	}
	public CustomerConnect() {
		this.initComponents();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
	}
	private void initComponents() {
		this.ipTxt = new JTextField();
		this.portTxt = new JTextField();
		JLabel title = new JLabel("Connect to a store");
		JLabel ipLbl = new JLabel("IP Address");
		JLabel portLbl = new JLabel("Port");
		JButton connect = new JButton("Connect");
		
		Container mainPanel = this.getContentPane();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel connectGrid = new JPanel();
		connectGrid.setLayout(new GridLayout(2, 2));
		
		connectGrid.add(ipLbl);
		connectGrid.add(ipTxt);
		connectGrid.add(portLbl);
		connectGrid.add(portTxt);
		
		mainPanel.add(title);
		mainPanel.add(connectGrid);
		mainPanel.add(connect);
		
		connect.addActionListener(p -> {
			Client.getInstance().setClient(new StoreClient(ipTxt.getText(), Integer.parseInt(portTxt.getText())));
			try {
				Client.getInstance().getClient().openConnection();
				CustomerMain.addText("Successfully connected to " + ipTxt.getText());
				CustomerMain.console.setBackground(Color.GREEN);
				CustomerMain.connect.setEnabled(false);
				CustomerMain.disconnect.setEnabled(true);
				Client.getInstance().requestStock();
				this.dispose();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Cannot connect to the store. Please check again.");
				System.out.println(e.toString());
			}
		});
		
		
	}


}
